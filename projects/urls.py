from django.urls import path
from projects.views import projects_list, projects_details, create_project

urlpatterns = [
    path("", projects_list, name="list_projects"),
    path("<int:id>/", projects_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
