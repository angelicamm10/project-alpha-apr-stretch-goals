from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm


# Create your views here.
@login_required
def projects_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects_list": projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def projects_details(request, id):
    project = Project.objects.get(id=id)
    context = {"project": project}
    return render(request, "projects/detail_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
        return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
